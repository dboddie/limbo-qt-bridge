# pixmaps.b
#
# Written in 2018 by David Boddie <david@boddie.org.uk>
#
# To the extent possible under law, the author(s) have dedicated all copyright
# and related and neighboring rights to this software to the public domain
# worldwide. This software is distributed without any warranty.
#
# You should have received a copy of the CC0 Public Domain Dedication along with
# this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.

# Tests an integration bridge between Limbo and Qt.

implement Pixmaps;

# Import modules to be used and declare any instances that will be accessed
# globally.

include "draw.m";

include "sys.m";
    sys: Sys;
    fprint, print, sprint: import sys;

include "qtwidgets.m";
    qt: QtWidgets;
    QApplication, QImage, QLabel, QPixmap, QGridLayout, Qt, QWidget: import qt;
    connect, forget: import qt;

Pixmaps: module
{
    init: fn(ctxt: ref Draw->Context, args: list of string);
};

init(ctxt: ref Draw->Context, args: list of string)
{
    sys = load Sys Sys->PATH;
    qt = load QtWidgets QtWidgets->PATH;

    qt->init();
    app := QApplication.new();

    window := QWidget.new();

    data := array[16 * 16 * 4] of byte;

    for (i := 0; i < 16; i++) {
        for (j := 0; j < 16; j++) {
            r := i * 15;
            g := 128;
            b := j * 15;
            data[(((i * 16) + j) * 4) + 3] = byte 255;
            data[(((i * 16) + j) * 4) + 2] = byte r;
            data[(((i * 16) + j) * 4) + 1] = byte g;
            data[(((i * 16) + j) * 4)] = byte b;
        }
    }

    layout := QGridLayout.new();
    label := QLabel.new();
    image := QImage.newFromData(data, 16, 16, QImage.ARGB32);
    image = image.scaled(256, 256, Qt.IgnoreAspectRatio, Qt.FastTransformation);
    pixmap := QPixmap.fromImage(image, Qt.AutoColor);
    label.setPixmap(pixmap);
    layout.addWidget(label, 0, 0, 0, 0);

    window.setLayout(layout);
    window.setWindowTitle("Limbo to Qt Bridge Pixmaps Demonstration");
    window.show();
}
