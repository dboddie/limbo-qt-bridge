include "tables.m";
    tables: Tables;
    Strhash, Table: import tables;

QtChannels: module
{
    PATH: con "/dis/lib/qtchannels.dis";

    bytearray: type array of byte;

    Channels: adt {
        counter: int;
        persistent_counter: int;
        response_hash : ref Table[chan of bytearray];
        read_ch : chan of bytearray;
        write_ch : chan of bytearray;

        init: fn(): ref Channels;
        get: fn(c: self ref Channels): (int, chan of bytearray);
        get_persistent: fn(c: self ref Channels): (int, chan of bytearray);
        reader: fn(c: self ref Channels);
        writer: fn(c: self ref Channels);
        request: fn(c: self ref Channels, action: bytearray,
                    args: list of bytearray,
                    return_value_expected: int): bytearray;
    };

    ArgList: type list of bytearray;

    enc: fn(s, t: string): bytearray;
    enc_str: fn(s: string): bytearray;
    enc_bytes: fn(b: bytearray): bytearray;
    enc_int: fn(i: int): bytearray;
    enc_real: fn(r: real): bytearray;
    enc_bool: fn(i: int): bytearray;
    enc_enum: fn(name: string, value: int): bytearray;
    enc_value: fn(name: string, values: list of bytearray): bytearray;
    enc_inst: fn[T](instance: T): bytearray
        for { T => _get_proxy: fn(w: self T): string; };
    parse_arg: fn(b: bytearray): (string, bytearray, bytearray);
    dec_str: fn(b: bytearray): string;
    dec_int: fn(b: bytearray): int;
    dec_bool: fn(b: bytearray): int;
    parse_2tuple: fn(b: bytearray): (string, string);
    parse_args: fn(b: bytearray): ArgList;
    join_arrays: fn(arrays: list of bytearray): bytearray;

    debug_msg: fn(s: string);
};
