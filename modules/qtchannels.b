# qtchannels.b
#
# Copyright (c) 2018, David Boddie <david@boddie.org.uk>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

implement QtChannels;

include "lists.m";
    lists: Lists;

include "string.m";
    str: String;

include "sys.m";
    sys: Sys;
    fprint, print, sprint: import sys;

include "qtchannels.m";

consctl : ref sys->FD;
f : ref sys->FD;

Channels.init(): ref Channels
{
    sys = load Sys Sys->PATH;
    str = load String String->PATH;
    lists = load Lists Lists->PATH;
    tables = load Tables Tables->PATH;
    f = sys->create("tmp.txt", sys->OWRITE, 8r666);
    
    # Enable raw mode so that characters written to stdin are not automatically
    # echoed back to stdout without us seeing them.
    consctl = sys->open("/dev/consctl", sys->OWRITE);
    sys->write(consctl, bytearray "rawon\n", 6);

    response_hash := Table[chan of bytearray].new(7, nil);
    read_ch := chan of bytearray;
    write_ch := chan of bytearray;

    c := ref Channels(0, 0, response_hash, read_ch, write_ch);

    # Spawn a reader and a writer to handle input and output in the background.
    spawn c.reader();
    spawn c.writer();

    return c;
}

Channels.get(c: self ref Channels): (int, chan of bytearray)
{
    # Creates a new channel and registers it in the hash table.
    response_ch := chan of bytearray;

    c.counter = (c.counter + 1) % 1024;
    c.response_hash.add(c.counter, response_ch);

    return (c.counter, response_ch);
}

Channels.get_persistent(c: self ref Channels): (int, chan of bytearray)
{
    # Creates a new channel and registers it in the hash table.
    response_ch := chan of bytearray;

    c.persistent_counter = (c.persistent_counter + 1) % 1024;
    c.response_hash.add(1024 + c.persistent_counter, response_ch);

    return (1024 + c.persistent_counter, response_ch);
}

Channels.reader(c: self ref Channels)
{
    stdin := sys->fildes(0);
    # Use a fixed buffer for reading from stdin and an extensible one for
    # accumulating bytes.
    read_array := array[256] of byte;
    current: bytearray;
    # Looking for the start of a message. Expected at least one byte.
    in_message := 0;
    input_expected := 1;

    for (;;) {

        # Read as much as possible from stdin.
        read := sys->read(stdin, read_array, 256);

        if (read == 0) {
            debug_msg("Application's stdin closed.");
            exit;
        }

        # Convert the input to a string and append it to the current string.
        new_array := array[len current + read] of byte;
        for (i := 0; i < len current; i++)
            new_array[i] = current[i];

        for (i = 0; i < read; i++)
            new_array[len current + i] = read_array[i];

        current = new_array;

        # Handle multiple commands while there is enough data to process.
        while (len current >= input_expected) {

            type_: string;
            token, rest: bytearray;
            value_str: bytearray;

            if (in_message == 0) {

                # Find a number followed by a space.
                i := 0;
                while (i < len current && current[i] != byte 32)
                    i++;

                # If no space is found then read again.
                if (i == len current)
                    break;

                token = current[:i];
                rest = current[i + 1:];

                # Convert the length string to a number.
                input_expected = int string token;

                # Examine the rest of the input.
                current = rest;
                in_message = 1;
            }

            # Try to read the rest of the message.
            if (len current >= input_expected) {

                value_str := current[:input_expected];

                (type_, token, value_str) = parse_arg(value_str);
                if (type_ != "s") {
                    errstr := sprint("Read error: '%s' '%s' '%s'\n", type_, string token, string value_str);
                    fprint(sys->fildes(2), "%d %s", len errstr, errstr);
                    exit;
                }

                (type_, token, value_str) = parse_arg(value_str);
                if (type_ != "i") {
                    errstr := sprint("Read error: '%s' '%s' '%s'\n", type_, string token, string value_str);
                    fprint(sys->fildes(2), "%d %s", len errstr, errstr);
                    exit;
                }

                ch := c.response_hash.find(int string token);

                if (ch != nil) {
                    # Send the command via the response channel.
                    ch <-= value_str;
                } else {
                    # Send the command via the default read channel.
                    c.read_ch <-= value_str;
                }

                current = current[input_expected:];
                in_message = 0;
                input_expected = 1;

            } else {
                # Not enough data for the message body so read again.
                break;
            }
        }
    }
}

Channels.writer(c: self ref Channels)
{
    stdout := sys->fildes(1);

    for (;;) {

        # Read a byte array from the write channel.
        message_array := <- c.write_ch;
        message_length := len message_array;

    sys->write(f, message_array, message_length);

        # Prefix the message with its length and append a newline to it.
        length_array := bytearray (string message_length + " ");
        length_length := len length_array;

        # Write the length and space to stdout.
        if (sys->write(stdout, length_array, length_length) != length_length) {
            fprint(sys->fildes(2), "Write error.\n");
            exit;
        }

        # Write the message to stdout.
        if (sys->write(stdout, message_array, message_length) != message_length) {
            fprint(sys->fildes(2), "Write error.\n");
            exit;
        }
    }
}

Channels.request(c: self ref Channels, action: bytearray, args: list of bytearray,
                 return_value_expected: int): bytearray
{
    # Obtain a channel to use to receive a response.
    (id_, response_ch) := c.get();

    pieces := array[2 + len args] of bytearray;

    pieces[0] = action;
    pieces[1] = enc_int(id_);

    l := len action + len pieces[1];
    i := 2;

    for (; args != nil; args = tl args) {
        arg := hd args;
        pieces[i++] = arg;
        l += len arg;
    }

    message := array[l] of byte;
    i = 0;

    for (p := 0; p < len pieces; p++) {
        piece := pieces[p];
        for (j := 0; j < len piece; j++) {
            message[i++] = piece[j];
        }
    }

    if (message[len message - 1] == byte ' ')
        message[len message - 1] = byte '\n';

    # Send the call request and receive the response.
    c.write_ch <-= message;

    value: bytearray;
    if (return_value_expected)
        value = <- response_ch;

    # Delete the entry for the response in the response hash.
    c.response_hash.del(id_);

    return value;
}

enc(s, t: string): bytearray
{
    b := bytearray(t + (string len s) + " " + s + " ");
    return b;
}

enc_str(s: string): bytearray
{
    return enc(s, "s");
}

enc_bytes(b: bytearray): bytearray
{
    ba := bytearray("b" + (string len b) + " ");
    return join_arrays(ba::b::(bytearray " ")::nil);
}

enc_int(i: int): bytearray
{
    s := string i;
    return enc(s, "i");
}

enc_real(r: real): bytearray
{
    s := string r;
    return enc(s, "f");
}

enc_bool(i: int): bytearray
{
    if (i != 0)
        return enc("True", "B");
    else
        return enc("False", "B");
}

enc_enum(name: string, value: int): bytearray
{
    # Create a pair of encoded values: C<length> <name> i<length> <value>
    ba := join_arrays(enc(name, "C")::enc_int(value)::nil);
    # Wrap them in an enum value specifier: e<length> ...
    s := sprint("v%d ", len ba);
    return join_arrays((bytearray s)::ba::(bytearray " ")::nil);
}

enc_value(name: string, values: list of bytearray): bytearray
{
    # Wrap the encoded values in a value class specifier: v<length> ...
    ba := enc(name, "C");
    for (; values != nil; values = tl values)
        ba = join_arrays(ba::(hd values)::nil);

    s := sprint("v%d ", len ba);
    return join_arrays((bytearray s)::ba::(bytearray " ")::nil);
}

enc_inst[T](instance: T): bytearray
    for { T => _get_proxy: fn(w: self T): string; }
{
    return enc(instance._get_proxy(), "I");
}

parse_arg(b: bytearray): (string, bytearray, bytearray)
{
    # Obtain the type and length.
    type_ := string b[:1];
    token: bytearray;

    i := 1;
    while (i < len b) {
        if (b[i] == byte 32)
            break;
        i++;
    }

    token = b[1:i];
    length := int string token;

    # Return the type, argument and remaining string.
    token = b[i + 1:i + 1 + length];
    return (type_, token, b[i + length + 2:]);
}

dec_str(b: bytearray): string
{
    (type_, token, rest) := parse_arg(b);
    return string token;
}

dec_int(b: bytearray): int
{
    (type_, token, rest) := parse_arg(b);
    return int string token;
}

dec_bool(b: bytearray): int
{
    (type_, token, rest) := parse_arg(b);
    if (string token == "True")
        return 1;
    else
        return 0;
}

parse_2tuple(b: bytearray): (string, string)
{
    type_: string;
    contents, token0, token1: bytearray;

    (type_, contents, b) = parse_arg(b);
    (type_, token0, contents) = parse_arg(contents);
    (type_, token1, contents) = parse_arg(contents);
    return (string token0, string token1);
}

parse_args(b: bytearray): ArgList
{
    l: list of bytearray;
    type_: string;
    token: bytearray;

    while (len b > 0) {
        (type_, token, b) = parse_arg(b);
        l = token::l;
    }

    return lists->reverse(l);
}

debug_msg(s: string)
{
    msg := sprint("s5 debug i4 9999 s%d '%s'", len s + 2, s);
    sys->print("%d %s", len msg, msg);
}

join_arrays(arrays: list of bytearray): bytearray
{
    l := 0;
    al := arrays;
    for (; al != nil; al = tl al)
        l += len hd al;

    ba := array[l] of byte;
    i := 0;

    for (; arrays != nil; arrays = tl arrays) {
        a := hd arrays;
        for (j := 0; j < len a; j++)
            ba[i++] = a[j];
    }

    return ba;
}
